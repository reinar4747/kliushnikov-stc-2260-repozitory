package hw6;

public class HW6 {
    /*
    Реализовать класс со статическими методами:

* Сложение всех чисел, которые передаются в метод. Метод возвращает результат сложения.

* Вычитание всех чисел, которые передаются в метод. Метод возвращает результат вычитания.
	* Доп. задание: найти наибольшее число из чисел, которые передали в метод. И производить вычитание из него.

* Умножение всех чисел, которые передаются в метод. Метод возвращает результат умножения.

* Деление всех чисел, которые передаются в метод. При каждом следующем делении должна идти проверка:
	* что делимое число должно быть больше делителя. Если условие не выполнено, то метод возвращает текущий результат деления.
	* что делитель - положительное число. Если условие не выполнено, то метод возвращает текущий результат деления.

* Метод, высчитывающий факториал переданного числа. Должна быть проверка, что переданное число положительное.
     */
    public static int addition(int[] arr) {
        int summ = 0;
        for (int i = 0; i < arr.length; i++) {
            summ += arr[i];
        }

        return summ;
    }

    public static int subtraction(int[] arr) {
        int max = arr[0];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                index = i;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (i == index) {
                continue;
            } else {
                max -= arr[i];
            }
        }

        return max;
    }

    public static int multiplication(int[] arr) {
        int mult = 1;
        for (int i = 0; i < arr.length; i++) {
            mult *= arr[i];
        }

        return mult;
    }

    public static double division(int[] arr) {
        int max = arr[0];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                index = i;
            }
        }
        double div = max;
        for (int i = 0; i < arr.length; i++) {
            if (index == i) {
                continue;
            }
            if (div < arr[i] || arr[i] < 0) {
                return div;
            } else {
                div /= arr[i];
            }
        }

        return div;
    }

    public static int factorial(int number) {
        if (number > 0) {
            int result = number;
            for (int i = number - 1; i > 1; i--) {
                result *= i;
            }
            return result;
        }

        return -1;
    }

    public static void main(String[] args) {
        int[] test = {1, 2, 3, 10, 5, 6, 7, 8, 9};
        int[] test1 = {1, 3, 5, 6, 7, 8, 1000};

        System.out.println(addition(test));

        System.out.println(subtraction(test));

        System.out.println(multiplication(test));

        System.out.println(division(test1));

        System.out.println(factorial(6));

    }
}
