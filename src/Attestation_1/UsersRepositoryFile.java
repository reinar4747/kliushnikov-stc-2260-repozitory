package Attestation_1;

public interface UsersRepositoryFile {

    void create(User user);

    User findById(int id);

    void update(User user);

    void delete(int id);

}
