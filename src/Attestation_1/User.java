package Attestation_1;

public class User {
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private boolean hasWork;

    public User(String firstName, String lastName, int age, boolean hasWork) {
        this.id = UsersRepositoryFileImpl.generateId();
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.hasWork = hasWork;
    }

    public User(int id, String firstName, String lastName, int age, boolean hasWork) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.hasWork = hasWork;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getHasWork() {
        return hasWork;
    }

    public void setHasWork(boolean hasWork) {
        this.hasWork = hasWork;
    }

    public void setHasWork(String work) {
        if (work.equals("Работает")) {
            this.hasWork = true;
        } else if (work.equals("Безработный")) {
            this.hasWork = false;
        }
    }

    @Override
    public String toString() {
        return String.format("ID : %d\n" +
                        "First Name : %s\n" +
                        "Last Name : %s\n" +
                        "Age : %d\n" +
                        "Work : %s",
                id, firstName,
                lastName, age,
                hasWork ? "Работает" : "Безработный");
    }
}
