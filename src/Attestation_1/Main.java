package Attestation_1;

public class Main {
    public static void main(String[] args) {

        User user1 = new User("Vasa", "Pupkin", 34, true);
        User user2 = new User("Petya", "Vovkin", 45, false);
        User user3 = new User("Fedya", "Fedotov", 23, true);
        User user4 = new User("Artem", "Anisimov", 18, false);

        UsersRepositoryFileImpl usersRepositoryFile = new UsersRepositoryFileImpl();


        usersRepositoryFile.create(user1);
        usersRepositoryFile.create(user2);
        usersRepositoryFile.create(user3);
        usersRepositoryFile.create(user4);

        User vasa = usersRepositoryFile.findById(0);
        System.out.println(vasa);

        vasa.setFirstName("Vasiliy");
        vasa.setAge(45);
        usersRepositoryFile.update(vasa);

        User vasiliy = usersRepositoryFile.findById(0);
        System.out.println(vasiliy);

        usersRepositoryFile.delete(0);


    }
}
