package Attestation_1;

import java.io.*;
import java.util.Scanner;

public class UsersRepositoryFileImpl implements UsersRepositoryFile {

    private static final String USERS_REPOSITORY_PATH = "src/Attestation_1";
    private static final String USERS_REPOSITORY = USERS_REPOSITORY_PATH + "/Users.txt";
    private static final String USERS_REPOSITORY_TEMP = USERS_REPOSITORY_PATH + "/tempUsers.txt";
    private static final String USER_ID = "src/Attestation_1/User_ID.txt";

    public void create(User user) {
        try (Writer writer = new FileWriter(USERS_REPOSITORY, true)) {
            writeToFile(writer, userToStringArray(user));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(String.format("User : %s %s successfully added to file!",
                user.getLastName(), user.getFirstName()));
    }

    public User findById(int id) {
        try (Scanner scanner = new Scanner(new File(USERS_REPOSITORY))) {
            while (scanner.hasNext()) {
                String[] arrLine = scanner.nextLine().split("\\|");
                if (Integer.parseInt(arrLine[0]) == id) {
                    return new User(Integer.parseInt(arrLine[0]),
                            arrLine[1],
                            arrLine[2],
                            Integer.parseInt(arrLine[3]),
                            Boolean.parseBoolean(arrLine[4]));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println("This id doesn't exist!");
        return null;
    }

    public void update(User user) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(USERS_REPOSITORY_TEMP, true));
            Scanner scanner = new Scanner(new File(USERS_REPOSITORY));
            File tempFile = new File(USERS_REPOSITORY_TEMP);
            File file = new File(USERS_REPOSITORY);

            tempFile.createNewFile();
            while (scanner.hasNext()) {
                String[] arrLine = scanner.nextLine().split("\\|");
                if (Integer.parseInt(arrLine[0]) != user.getId()) {
                    writeToFile(writer, arrLine);
                } else {
                    writeToFile(writer, userToStringArray(user));
                    System.out.println(String.format("User : %s %s successfully updated!",
                            user.getLastName(), user.getFirstName()));
                }
            }
            closeAll(writer, scanner, file, tempFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(int id) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(USERS_REPOSITORY_TEMP, true));
            Scanner scanner = new Scanner(new File(USERS_REPOSITORY));
            File tempFile = new File(USERS_REPOSITORY_TEMP);
            File file = new File(USERS_REPOSITORY);

            tempFile.createNewFile();
            while (scanner.hasNext()) {
                String[] arrLine = scanner.nextLine().split("\\|");
                if (Integer.parseInt(arrLine[0]) != id) {
                    writeToFile(writer, arrLine);
                } else {
                    System.out.println(String.format("User : %s %s successfully deleted!",
                            arrLine[2], arrLine[1]));
                }
            }
            closeAll(writer, scanner, file, tempFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static int generateId() {
        int id;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USER_ID));
            id = Integer.parseInt(reader.readLine()) + 1;
            reader.close();
            BufferedWriter writer = new BufferedWriter(new FileWriter(USER_ID));
            writer.write("" + id);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return id;
    }

    private void writeToFile(Writer writer, String[] userArr) throws IOException {
        writer.write(String.format("%s|%s|%s|%s|%s\n",
                userArr[0],
                userArr[1],
                userArr[2],
                userArr[3],
                userArr[4]));
        writer.flush();
    }

    private String[] userToStringArray(User user) {
        return new String[]{"" + user.getId(),
                user.getFirstName(),
                user.getLastName(),
                "" + user.getAge(),
                "" + user.getHasWork()};
    }

    private void closeAll(Writer writer, Scanner scanner, File file, File tempFile) throws IOException {
        writer.close();
        scanner.close();
        file.delete();
        tempFile.renameTo(file);
    }
}




