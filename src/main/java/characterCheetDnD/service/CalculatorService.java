package characterCheetDnD.service;

import characterCheetDnD.dto.CalculatorDto;
import characterCheetDnD.models.Calculator;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Transactional
public interface CalculatorService {
    Calculator saveAbilityScores(long characterId);
    List<Calculator> getAllById(long id);
    void setDefaultAbilityScoreValues(long id);
    void getNewAbilityScoresSet();
    void strengthButton(CalculatorDto calculatorDto, Calculator calculatorStrength,long id);
    void dexterityButton(CalculatorDto calculatorDto, Calculator calculatorDexterity,long id);
    void constitutionButton(CalculatorDto calculatorDto, Calculator calculatorConstitution,long id);
    void intelligenceButton(CalculatorDto calculatorDto, Calculator calculatorIntelligence,long id);
    void wisdomButton(CalculatorDto calculatorDto, Calculator calculatorWisdom,long id);
    void charismaButton(CalculatorDto calculatorDto, Calculator calculatorCharisma,long id);

}
