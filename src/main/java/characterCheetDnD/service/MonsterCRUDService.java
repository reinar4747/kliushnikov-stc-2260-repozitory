package characterCheetDnD.service;

import characterCheetDnD.models.Monster;
import characterCheetDnD.models.UserAccount;

import java.util.List;

public interface MonsterCRUDService {
//    -------------Crud ---------------------
    List<Monster> getMonsterById(long monsterId);
    List<Monster> getMonsterByUserId(UserAccount userAccount);

    void onAndOffMonsterById(long id,boolean flag);

    List<Monster> getAllByMonsterName(String monsterName);

    void saveMonster(long monsterId);
    List<Monster> monsters();
    //    --------------- create -------------
    void createNewMonster();
    //---------------delete---------------
    void deleteMonsterById(long monsterId);
    void logicalDeleteMonsterById(boolean flag, long monsterId);

    //    ----------- name and type ----------
    void setMonsterNameIntoDB(String name,long monsterId);

    void setMonsterTypeIntoDB(String monsterType,long monsterId);

    //    --------------- monster stats --------------
    void setMonsterStatsIntoDB(String monsterStats,long monsterId);
    void setMonsterStrengthIntoDB(String monsterStr,long monsterId);

    void setMonsterDexterityIntoDB(String monsterDex,long monsterId);

    void setMonsterConstitutionIntoDB(String monsterCon,long monsterId);

    void setMonsterIntelligenceIntoDB(String monsterInt,long monsterId);

    void setMonsterWisdomIntoDB(String monsterWis,long monsterId);

    void setMonsterCharismaIntoDB(String monsterCha,long monsterId);

    //    ---------------------- description ------------------
    void setMonsterDescriptionIntoDB(String monsterDescription,long monsterId);

    long tempMonsterId();

}
