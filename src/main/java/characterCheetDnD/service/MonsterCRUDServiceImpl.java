package characterCheetDnD.service;

import characterCheetDnD.models.GameCharacter;
import characterCheetDnD.models.Monster;
import characterCheetDnD.models.UserAccount;
import characterCheetDnD.repositories.MonsterRepository;
import characterCheetDnD.repositories.UserAccountRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Data
@Service
public class MonsterCRUDServiceImpl implements MonsterCRUDService {

    private long tempMonsterId;
    UserAccountRepository userAccountRepository;
    MonsterRepository monsterRepository;
    AccountService accountService;

    List<Monster> monsterToShow;

    @Autowired
    public MonsterCRUDServiceImpl(UserAccountRepository userAccountRepository,
                                  MonsterRepository monsterRepository,
                                  AccountService accountService) {
        this.userAccountRepository = userAccountRepository;
        this.monsterRepository = monsterRepository;
        this.accountService = accountService;
    }

    public long tempMonsterId() {

        return tempMonsterId = monsterRepository.getAllByMonsterName("someEvilMonster").get(0).getId();
    }

    @Override
    public List<Monster> getMonsterById(long monsterId) {
        return monsterRepository.getAllById(monsterId);
    }

    @Override
    public List<Monster> getMonsterByUserId(UserAccount userAccount) {
       return monsterRepository.getAllByUserAccountsAndMonsterDeleted(userAccount,false);
    }

    @Override
    public void onAndOffMonsterById(long id, boolean flag) {
        monsterRepository.setMonsterOnAndOff(flag,id);
    }

    @Override
    public List<Monster> getAllByMonsterName(String monsterName) {
        return monsterRepository.getAllByMonsterName(monsterName);
    }

    @Override
    @Transactional
    public void saveMonster(long monsterId) {
        monsterToShow = getMonsterById(monsterId);
    }
    @Override
    @Transactional
    public List<Monster> monsters() {
        return monsterToShow;
    }



    @Override
    @Transactional
    public void createNewMonster() {
        Monster newMonster = Monster.builder()
                .monsterName("someEvilMonster")
                .monsterType("-")
                .monsterStrength("10")
                .monsterDexterity("10")
                .monsterConstitution("10")
                .monsterIntelligence("10")
                .monsterWisdom("10")
                .monsterCharisma("10")
                .monsterDescription("-")
                .monsterDeleted(false)
                .build();

        UserAccount loggedUser = userAccountRepository
                .getAllByEmailAndUserDeleted(accountService
                        .getLoggedUser()
                        .get(0)
                        .getEmail(), false)
                .get(0);

        loggedUser.getMonsters().add(newMonster);

        userAccountRepository.save(loggedUser);


    }

    @Override
    @Transactional
    public void deleteMonsterById(long monsterId) {
        monsterRepository.deleteMonstersById(monsterId);
    }
    @Override
    @Transactional
    public void logicalDeleteMonsterById(boolean flag, long monsterId){
        monsterRepository.setMonsterOnAndOff(flag,monsterId);
    }


    @Override
    public void setMonsterNameIntoDB(String name, long monsterId) {
        monsterRepository.setMonsterNameIntoDB(name, monsterId);
    }

    @Override
    public void setMonsterTypeIntoDB(String monsterType, long monsterId) {
        monsterRepository.setMonsterTypeIntoDB(monsterType, monsterId);
    }

    public void setMonsterStatsIntoDB(String monsterStats, long monsterId) {

        String[] monsterAbilityScores = new String[6];
        for (int i = 0; i < monsterStats.split(",").length; i++) {
            monsterAbilityScores[i] = monsterStats.split(",")[i];
        }
        if (monsterAbilityScores[0] != null) {
            monsterRepository.setMonsterStrIntoDB(monsterAbilityScores[0], monsterId);
        }
        if (monsterAbilityScores[1] != null) {
            monsterRepository.setMonsterDexIntoDB(monsterAbilityScores[1], monsterId);
        }
        if (monsterAbilityScores[2] != null) {
            monsterRepository.setMonsterConIntoDB(monsterAbilityScores[2], monsterId);
        }
        if (monsterAbilityScores[3] != null) {
            monsterRepository.setMonsterIntIntoDB(monsterAbilityScores[3], monsterId);
        }
        if (monsterAbilityScores[4] != null) {
            monsterRepository.setMonsterWisIntoDB(monsterAbilityScores[4], monsterId);
        }
        if (monsterAbilityScores[5] != null) {
            monsterRepository.setMonsterChaIntoDB(monsterAbilityScores[5], monsterId);
        }

    }


    @Override
    public void setMonsterStrengthIntoDB(String monsterStr, long monsterId) {
        monsterRepository.setMonsterStrIntoDB(monsterStr, monsterId);
    }

    @Override
    public void setMonsterDexterityIntoDB(String monsterDex, long monsterId) {
        monsterRepository.setMonsterDexIntoDB(monsterDex, monsterId);
    }

    @Override
    public void setMonsterConstitutionIntoDB(String monsterCon, long monsterId) {
        monsterRepository.setMonsterConIntoDB(monsterCon, monsterId);
    }

    @Override
    public void setMonsterIntelligenceIntoDB(String monsterInt, long monsterId) {
        monsterRepository.setMonsterIntIntoDB(monsterInt, monsterId);
    }

    @Override
    public void setMonsterWisdomIntoDB(String monsterWis, long monsterId) {
        monsterRepository.setMonsterWisIntoDB(monsterWis, monsterId);
    }

    @Override
    public void setMonsterCharismaIntoDB(String monsterCha, long monsterId) {
        monsterRepository.setMonsterChaIntoDB(monsterCha, monsterId);
    }

    @Override
    public void setMonsterDescriptionIntoDB(String monsterDescription, long monsterId) {
        monsterRepository.setMonsterDescriptionIntoDB(monsterDescription, monsterId);
    }


}
