package characterCheetDnD.validators;

public interface EmailValidator {
    String validate(String email);
}
