package characterCheetDnD.validators;

import org.springframework.stereotype.Component;

@Component("passwordByLowerCase")
public class PasswordValidatorByLowerCase implements PasswordValidator{
    @Override
    public String validate(String password,String passwordRepeat) {
        int flag = 0;
        for (char point : password.toCharArray()) {
            if (Character.isLowerCase(point)) {
                flag++;
                break;
            }
        }
        if (flag == 0){
            return "Password doesn't has lower letters \n";
        }else {
            return "";
        }
    }
}
