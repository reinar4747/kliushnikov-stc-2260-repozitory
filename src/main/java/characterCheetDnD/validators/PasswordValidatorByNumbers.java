package characterCheetDnD.validators;

import org.springframework.stereotype.Component;

@Component("passwordByNumbers")
public class PasswordValidatorByNumbers implements PasswordValidator {
    @Override
    public String validate(String password,String passwordRepeat) {
        int flag = 0;
        String[] numbers = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};
        for (String number : numbers) {
            if (password.contains(number)) {
                flag++;
                break;
            }
        }
        if (flag == 0){
            return "Password doesn't has numbers";
        }else {
            return "";
        }

    }
}
