package characterCheetDnD.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("emailByLength")
public class EmailValidatorByLength implements EmailValidator {

    private int minLength;

    @Autowired
    public EmailValidatorByLength(@Value("${validator.email.length}") int minLength) {
        this.minLength = minLength;
    }

    @Override
    public String validate(String email) {
        if (email.length() < minLength) {
            return "Длина почты слишком мала - " + email.length() + " a нужно - " + minLength + " \n";
        }
        return "";
    }
}
