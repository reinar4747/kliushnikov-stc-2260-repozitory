package characterCheetDnD.controllers.entitiesControllers;

import characterCheetDnD.dto.CharacterSheetDto;
import characterCheetDnD.models.Armor;
import characterCheetDnD.models.Calculator;
import characterCheetDnD.models.GameCharacter;
import characterCheetDnD.models.Weapon;
import characterCheetDnD.service.AccountService;
import characterCheetDnD.service.CalculatorService;
import characterCheetDnD.service.CharacterCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@Transactional
@RequestMapping("/characterSelect/characterSheetLook")
public class CharacterSheetController {
    AccountService accountService;
    CharacterCRUDService characterCRUDService;
    CalculatorService calculatorService;

    long characterId;// =

    @Autowired
    public CharacterSheetController(AccountService accountService,
                                    CharacterCRUDService characterCRUDService,
                                    CalculatorService calculatorService) {
        this.accountService = accountService;
        this.characterCRUDService = characterCRUDService;
        this.calculatorService = calculatorService;

    }

    @GetMapping
    @Transactional
    public String getCharacterSheetPage(Model model) {

        characterId = characterCRUDService.character().get(0).getId();

        GameCharacter gameCharacter = characterCRUDService.getCharactersById(characterId).get(0);

        List<Weapon> weapons = gameCharacter.getWeapons();
        List<Armor> armors = gameCharacter.getArmors();
        List<Calculator> stats = Arrays.asList(gameCharacter.getAbilityScores());

        model.addAttribute("loggedUser", accountService.getLoggedUser());
        model.addAttribute("characterList", characterCRUDService.getCharactersById(characterId));
        model.addAttribute("characterWeaponList", weapons);
        model.addAttribute("characterArmorList", armors);
        model.addAttribute("characterAbilityScoresList", stats);
        model.addAttribute("maxHitPointsList", List.of(gameCharacter.getMaxHitPoints() * gameCharacter.getLevel()));

        return "/createModels/characterSheetLook";
    }

    @PostMapping
    public String ChangeCharacterSheetPage(CharacterSheetDto characterSheetDto) {
     //   long characterId = characterCRUDService.character().get(0).getId();
        if (characterSheetDto.getBackToCharacterSelect() != 0L) {

            return "redirect:/characterSelect";
        } else if (characterSheetDto.getGoToCharacterChange() != 0) {

            return "redirect:/characterSelect/characterSheetLook/characterSheetChange";
        } else if (characterSheetDto.getCharacterNewDataButton() != null)  {
            switch (characterSheetDto.getCharacterNewDataButton()) {
                case "newHP" ->
                        characterCRUDService.setHitPointsIntoDB(characterSheetDto.getNewHPIntoDB(), characterId);
                case "inventory" -> characterCRUDService.addItemIntoInventory(characterSheetDto.getNewInventoryItemIntoDB(),characterId);
            }
        }

        return "redirect:/characterSelect/characterSheetLook";
    }

    @GetMapping("/characterSheetChange")
    @Transactional
    public String getCharacterSheetChangePage(Model model) {
        long characterId = characterCRUDService.character().get(0).getId();

        model.addAttribute("loggedUser",accountService.getLoggedUser());
        model.addAttribute("characterList", characterCRUDService.getCharactersById(characterId));

        return "/createModels/characterSheetChange";
    }

    @PostMapping("/characterSheetChange")
    @Transactional
    public String changeData(CharacterSheetDto characterSheetDto) {
    //    long characterId = characterCRUDService.character().get(0).getId();

        int a =1;
        if (characterSheetDto.getCharacterNewDataButton() != null) {
            switch (characterSheetDto.getCharacterNewDataButton()) {
                case "newCharacterName" ->
                        characterCRUDService.setNameIntoDB(characterSheetDto.getNewCharacterNameIntoDB(), characterId);
                case "newCharacterLvL" ->
                        characterCRUDService.setLvLIntoDB(characterSheetDto.getNewCharacterLvLIntoDB(), characterId);
                case "newCharacterExp" ->
                        characterCRUDService.setExperienceIntoDB(characterSheetDto.getNewCharacterExpIntoDB(), characterId);
                case "newHP" ->
                        characterCRUDService.setHitPointsIntoDB(characterSheetDto.getNewHPIntoDB(), characterId);
            }
        } else if (characterSheetDto.getBackToCharacterSheet() != 0L) {

            return "redirect:/characterSelect/characterSheetLook";
        }


        return "redirect:/characterSelect/characterSheetLook/characterSheetChange";
    }

    @GetMapping("/charSheetWeaponSelection")
    public String getSelectWeaponPage(Model model) {

        List<Weapon> allWeapons = new ArrayList<>();
        allWeapons.addAll(characterCRUDService.getWeaponByEquipped("1h"));
        allWeapons.addAll(characterCRUDService.getWeaponByEquipped("2h"));

        model.addAttribute("weapons", allWeapons);

        return "createModels/charSheetWeaponSelection";
    }

    @PostMapping("/charSheetWeaponSelection")
    @Transactional
    public String selectWeapon(CharacterSheetDto characterSheetDto) {

     //   long characterId = characterCRUDService.character().get(0).getId();
        GameCharacter gameCharacter = characterCRUDService.getCharactersById(characterId).get(0);
        if (characterSheetDto.getChangeCharacterWeapon() != 0L) {
            characterCRUDService.setWeaponIntoDB(
                    characterCRUDService.getWeaponById(characterSheetDto.getChangeCharacterWeapon()).get(0).getName(), characterId);

            gameCharacter.getWeapons().add(characterCRUDService.getWeaponById(characterSheetDto.getChangeCharacterWeapon()).get(0));
        } else if (characterSheetDto.getDeleteCharacterWeapon() != 0L) {
            gameCharacter.getWeapons().remove(characterCRUDService.getWeaponById(characterSheetDto.getDeleteCharacterWeapon()).get(0));
        }

        return "redirect:/characterSelect/characterSheetLook";

    }

    @GetMapping("/charSheetArmourSelection")
    public String getSelectArmorPage(Model model) {

        List<Armor> heavy = new ArrayList<>();
        heavy.addAll(characterCRUDService.getAllByArmorWeight("heavy"));
        heavy.addAll(characterCRUDService.getAllByArmorWeight("mid"));
        heavy.addAll(characterCRUDService.getAllByArmorWeight("light"));
        model.addAttribute("armors", heavy);

        return "createModels/charSheetArmourSelection";
    }

    @PostMapping("/charSheetArmourSelection")
    @Transactional
    public String selectArmor(CharacterSheetDto characterSheetDto) {

     //   long characterId = characterCRUDService.character().get(0).getId();
        GameCharacter gameCharacter = characterCRUDService.getCharactersById(characterId).get(0);

        characterCRUDService.setArmourIntoDB(
                characterCRUDService.getArmorById(characterSheetDto.getChangeCharacterArmor()).get(0).getArmorName(), characterId);

        gameCharacter.getArmors().set(0, characterCRUDService.getArmorById(characterSheetDto.getChangeCharacterArmor()).get(0));

        return "redirect:/characterSelect/characterSheetLook";

    }



}
