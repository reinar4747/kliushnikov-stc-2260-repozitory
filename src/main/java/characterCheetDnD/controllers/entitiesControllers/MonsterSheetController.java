package characterCheetDnD.controllers.entitiesControllers;

import characterCheetDnD.dto.MonsterDto;
import characterCheetDnD.service.AccountService;
import characterCheetDnD.service.MonsterCRUDService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Getter
@Controller
@RequestMapping("/accountMaster/monsterSelect/monsterSheetLook")
public class MonsterSheetController {
    AccountService accountService;
    MonsterCRUDService monsterCRUDService;

    long monsterId;

    @Autowired
    public MonsterSheetController(AccountService accountService, MonsterCRUDService monsterCRUDService) {
        this.accountService = accountService;
        this.monsterCRUDService = monsterCRUDService;
    }

    @GetMapping()
    public String getMonsterChangePage(Model model) {

        monsterId = monsterCRUDService.monsters().get(0).getId();

        model.addAttribute("loggedUser", accountService.getLoggedUser());
        model.addAttribute("monsterList", monsterCRUDService.getMonsterById(monsterId));

        return "/createModels/monsterSheetLook";
    }

    // /accountMaster/monsterSelect/monsterSheetLook/monsterSheetTypeSelection
    @GetMapping("/monsterSheetTypeSelection")
    public String getSelectionMonsterTypePage(MonsterDto monsterDto) {

        return "/createModels/monsterSheetTypeSelection";
    }
    @PostMapping("/monsterSheetTypeSelection")
    public String selectionMonsterTypePage(MonsterDto monsterDto) {
        switch (monsterDto.getSelectMonsterType()) {
            case "undead" -> monsterCRUDService.setMonsterTypeIntoDB("Нежить", monsterId);
            case "humanoid" -> monsterCRUDService.setMonsterTypeIntoDB("Гуманоид", monsterId);
            case "beast" -> monsterCRUDService.setMonsterTypeIntoDB("Зверь", monsterId);
        }

        return "redirect:/accountMaster/monsterSelect/monsterSheetLook";
    }

    @PostMapping
    public String ChangeMonsterSheetPage(MonsterDto monsterDto) {
        //   long characterId = characterCRUDService.character().get(0).getId();
        if (monsterDto.getBackToMonsterSelect() != 0L) {

            return "redirect:/accountMaster/monsterSelect";
        } else if (monsterDto.getMonsterName() != null) {
            monsterCRUDService.setMonsterNameIntoDB(monsterDto.getMonsterName(), monsterId);
        } else if (monsterDto.getMonsterStat() != null) {
            monsterCRUDService.setMonsterStatsIntoDB(monsterDto.getMonsterStat(), monsterId);
        } else if (monsterDto.getMonsterNewDataButton() != null) {
            monsterCRUDService.setMonsterDescriptionIntoDB(monsterDto.getMonsterDescription(),monsterId);

        }

        return "redirect:/accountMaster/monsterSelect/monsterSheetLook";
    }
}
