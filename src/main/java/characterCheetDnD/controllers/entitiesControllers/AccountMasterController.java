package characterCheetDnD.controllers.entitiesControllers;

import characterCheetDnD.dto.AccountChangeDataDto;
import characterCheetDnD.models.UserAccount;
import characterCheetDnD.service.AccountService;
import characterCheetDnD.service.CalculatorService;
import characterCheetDnD.service.CharacterCRUDService;
import characterCheetDnD.service.MonsterCRUDService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/accountMaster")
@Getter
public class AccountMasterController {
    private final CharacterCRUDService characterCRUDService;
    private final CalculatorService calculatorService;
    private final AccountService accountService;
    private final MonsterCRUDService monsterCRUDService;

    @Autowired
    public AccountMasterController(CharacterCRUDService characterCRUDService,
                                   CalculatorService calculatorService,
                                   AccountService accountService,
                                   MonsterCRUDService monsterCRUDService) {
        this.characterCRUDService = characterCRUDService;
        this.calculatorService = calculatorService;
        this.accountService = accountService;
        this.monsterCRUDService = monsterCRUDService;
    }

    @GetMapping
    public String getAccountMasterPage(Model model) {
        //   characterCRUDService.createNewCharacter();
        calculatorService.setDefaultAbilityScoreValues(1L);

        List<String> masterNickName = accountService
                .getLoggedUser()
                .stream()
                .map(UserAccount::getNickName)
                .toList();

        model.addAttribute("masterNickName", masterNickName);
        return "modelPages/accountMaster";
    }


    @PostMapping
    public String masterAccountChange(AccountChangeDataDto accountChangeDataDto) {

        switch (accountChangeDataDto.getCreate()) {
            case "character" -> {
                characterCRUDService.onAndOffCharacterById(1L, false);
                characterCRUDService.setDefaultCharactersValues();

                return "redirect:/characterNPCCreation";
            }
            case "monster" -> {
                monsterCRUDService.createNewMonster();

                return "redirect:/monsterCreation";
            }
        }

        if (accountChangeDataDto.getDeleteUser() != null) {
            accountService.deleteUserById(accountService.getLoggedUser().get(0).getId());

            return "redirect:/deleteUserMessage";

        }

        return "modelPages/accountMaster";
    }




}
