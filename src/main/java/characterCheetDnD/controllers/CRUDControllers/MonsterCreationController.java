package characterCheetDnD.controllers.CRUDControllers;

import characterCheetDnD.dto.MonsterDto;
import characterCheetDnD.service.MonsterCRUDService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Getter
@Controller
@RequestMapping("/monsterCreation")
public class MonsterCreationController {
    MonsterCRUDService monsterCRUDService;
    private long tempMonsterId;

    @Autowired
    public MonsterCreationController(MonsterCRUDService monsterCRUDService) {
        this.monsterCRUDService = monsterCRUDService;
    }

    @GetMapping
    public String getMonsterCreationPage(Model model) {
        if (tempMonsterId == 0L) {
            tempMonsterId = monsterCRUDService.tempMonsterId();
        }

        model.addAttribute("monsterList", monsterCRUDService.getMonsterById(tempMonsterId));

        return "createModels/monsterCreation";
    }

    @GetMapping("/monsterTypeSelection")
    public String getMonsterTypeSelectionPage() {

        return "createModels/monsterTypeSelection";
    }
    @PostMapping("/monsterTypeSelection")
    public String getSelectionMonsterTypePage(MonsterDto monsterDto){
        switch (monsterDto.getSelectMonsterType()){
            case "undead" -> monsterCRUDService.setMonsterTypeIntoDB("Нежить",tempMonsterId);
            case "humanoid" -> monsterCRUDService.setMonsterTypeIntoDB("Гуманоид",tempMonsterId);
            case "beast" -> monsterCRUDService.setMonsterTypeIntoDB("Зверь",tempMonsterId);
        }

        return "redirect:/monsterCreation";
    }

    @PostMapping
    public String inputMonsterData(MonsterDto monsterDto) {
        if (monsterDto.getBackToMasterPage() != null) {
            monsterCRUDService.deleteMonsterById(tempMonsterId);
            return "redirect:/accountMaster";
        } else if (monsterDto.getMonsterName() != null) {
            monsterCRUDService.setMonsterNameIntoDB(monsterDto.getMonsterName(),tempMonsterId);
        } else if (monsterDto.getMonsterStat() != null){
            monsterCRUDService.setMonsterStatsIntoDB(monsterDto.getMonsterStat(),tempMonsterId);
        }else if (monsterDto.getSaveMonster() != null){
            monsterCRUDService.setMonsterDescriptionIntoDB(monsterDto.getMonsterDescription(),tempMonsterId);

            return "redirect:/monsterCreatedMessage";
        }

        return "redirect:/monsterCreation";

    }
}
