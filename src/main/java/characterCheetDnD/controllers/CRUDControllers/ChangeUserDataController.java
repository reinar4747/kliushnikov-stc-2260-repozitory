package characterCheetDnD.controllers.CRUDControllers;

import characterCheetDnD.dto.AccountChangeDataDto;
import characterCheetDnD.models.UserAccount;
import characterCheetDnD.service.AccountService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/changeUserData")
public class ChangeUserDataController {
    AccountService accountService;

    public ChangeUserDataController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public String getChangeUserDataPage(Model model) {

        long userId = accountService.getLoggedUser().get(0).getId();

        model.addAttribute("loggedUser", accountService.getUserById(userId,false));
        model.addAttribute("users", accountService.getUserById(userId,false));
        return "serviceModels/changeUserData";
    }

    @PostMapping
    public String userChangeData(AccountChangeDataDto accountChangeDataDto) {
        long userId = accountService.getLoggedUser().get(0).getId();
        if (accountChangeDataDto.getBackToAccountPage() != null) {
            switch (accountChangeDataDto.getBackToAccountPage()) {
                case "master" -> {
                    return "redirect:/accountMaster";
                }
                case "gamer" -> {
                    return "redirect:/accountGamer";
                }
                default -> {
                }
            }
        } else if (accountChangeDataDto.getNewDataButton() != null) {
            switch (accountChangeDataDto.getNewDataButton()) {
                case "FirstName" -> accountService.updateFirstName(userId, accountChangeDataDto);
                case "LastName" -> accountService.updateLastName(userId, accountChangeDataDto);
                case "NickName" -> accountService.updateNickName(userId, accountChangeDataDto);
                case "Email" -> accountService.updateEmail(userId, accountChangeDataDto);
                case "Password" -> {
                    if (accountService.updatePassword(accountChangeDataDto).size() != 0) {
                        return "redirect:/changePasswordError";
                    }
                }
            }
        }

        return "redirect:/changeUserData";
    }

}
