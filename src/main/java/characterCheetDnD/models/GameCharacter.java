package characterCheetDnD.models;

import lombok.*;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "game_character")
@ToString(exclude = {"weapons","armors","userAccount","abilityScores"})
public class GameCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private long id;
    @Column(name = "character_name")
    private String name;
    @Column(name = "character_race")
    private String race;
    @Column(name = "character_class")
    private String className;
    @Column(name = "experience")
    private int experience;
    @Column(name = "hit_points")
    private int hitPoints;
    @Column(name = "max_hitpoints")
    int maxHitPoints;
    @Column(name = "level")
    private int level;
    @Column(name = "character_weapon")
    private String weapon;
    @Column(name = "character_armor")
    private String armour;
    @Column(name = "is_deleted")
    private boolean characterDeleted;
    @Column(name = "inventory")
    private String inventory;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "game_character_weapon",
            joinColumns = @JoinColumn(name = "character_id"),
            inverseJoinColumns = @JoinColumn(name = "weapon_id"))
    @Builder.Default
    private List<Weapon> weapons = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "game_character_armor",
            joinColumns = @JoinColumn(name = "character_id"),
            inverseJoinColumns = @JoinColumn(name = "armor_id"))
    @Builder.Default
    private List<Armor> armors = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserAccount userAccount;

    @OneToOne
    @JoinColumn(name = "ability_score", referencedColumnName = "character_id")
    private Calculator abilityScores;



}
