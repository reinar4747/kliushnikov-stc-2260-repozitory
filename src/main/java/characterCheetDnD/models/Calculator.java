package characterCheetDnD.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "calculator")
public class Calculator {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private long id;

    @Column(name = "ability_score_points")
    private int abilityScorePoints;

    @Column(name = "strength_value")
    private int strengthValue;
    @Column(name = "strength_prefix")
    private String strengthPrefix;
    @Column(name = "strength_modifier")
    private int strengthModifier;

    @Column(name = "dexterity_value")
    private int dexterityValue;
    @Column(name = "dexterity_prefix")
    private String dexterityPrefix;
    @Column(name = "dexterity_modifier")
    private int dexterityModifier;

    @Column(name = "constitution_value")
    private int constitutionValue;
    @Column(name = "constitution_prefix")
    private String constitutionPrefix;
    @Column(name = "constitution_modifier")
    private int constitutionModifier;

    @Column(name = "intelligence_value")
    private int intelligenceValue;
    @Column(name = "intelligence_prefix")
    private String intelligencePrefix;
    @Column(name = "intelligence_modifier")
    private int intelligenceModifier;

    @Column(name = "wisdom_value")
    private int wisdomValue;
    @Column(name = "wisdom_prefix")
    private String wisdomPrefix;
    @Column(name = "wisdom_modifier")
    private int wisdomModifier;

    @Column(name = "charisma_value")
    private int charismaValue;
    @Column(name = "charisma_prefix")
    private String charismaPrefix;
    @Column(name = "charisma_modifier")
    private int charismaModifier;


}
