package characterCheetDnD.models;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "armor")
@ToString(exclude = "gameCharacter")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Armor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "armor_id")
    long armorId;
    @Column(name = "armor_name")
    String armorName;
    @Column(name = "armor_class")
    String armorClass;
    @Column(name = "armor_weight")
    String armorWeight;
    @ManyToMany(mappedBy = "armors")
    @Builder.Default
    List<GameCharacter> gameCharacter = new ArrayList<>();


}
