package characterCheetDnD.models;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_account")
public class UserAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "nick_name")
    private String nickName;
    @Column(name = "password")
    private String password;
    @Column(name = "user_role")
    private String userRole;
    @Column(name = "email")
    private String email;
    @Column(name = "is_deleted")
    boolean userDeleted;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userAccount")
    private List<GameCharacter> gameCharacters;
    @ManyToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY)
    @JoinTable(name = "user_account_game_monster",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "monster_id"))
    @Builder.Default
    private List<Monster> monsters = new ArrayList<>();


//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "airPlane")
//    private List<Passenger> passengers;

}

