package characterCheetDnD.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MonsterDto {

    String backToMasterPage;
    long backToMonsterSelect;

    String monsterName;
    String monsterStat;
    String selectMonsterType;

    String monsterDescription;

    String saveMonster;

    String monsterNewDataButton;


    long chowMonster;

    long deleteMonster;

}
