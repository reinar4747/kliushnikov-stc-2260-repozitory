package characterCheetDnD.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CharacterSheetDto {
    long backToCharacterSelect;
    long goToCharacterChange;
    long backToCharacterSheet;

    String characterNewDataButton;

    String newCharacterName;
    String newCharacterLvL;
    String newCharacterExp;
    String newHP;

    String newCharacterNameIntoDB;
    int newCharacterLvLIntoDB;
    int newCharacterExpIntoDB;
    int newHPIntoDB;

    long changeCharacterWeapon;
    long changeCharacterArmor;
    long deleteCharacterWeapon;
    String newInventoryItemIntoDB;
}
