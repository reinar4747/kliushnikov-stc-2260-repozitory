package characterCheetDnD.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import characterCheetDnD.models.UserAccount;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {
    private String firstName;
    private String lastName;
    private String nickName;
    private String email;
    private String userRole;
    private String password;
    private String passwordRepeat;


}
