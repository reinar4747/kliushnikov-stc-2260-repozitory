package characterCheetDnD.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountChangeDataDto {

    String deleteUser;

    String newDataButton;

    String newFirstName;

    String newLastName;

    String newNickName;

    String newEmail;

    String oldPassword;
    String newPassword;
    String newPasswordRepeat;

    String display;
    String create;

    String backToAccountPage;




}
