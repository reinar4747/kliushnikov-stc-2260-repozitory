package characterCheetDnD.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CharacterCRUDDto {

    String selectRace;
    String selectClass;

    long selectWeapon;
    long selectArmor;

    String characterName;

    String backToAccount;

    long chowCharacter;

    String saveCharacter;
    String deleteNewCharacter;
    long deleteCharacter;





}
