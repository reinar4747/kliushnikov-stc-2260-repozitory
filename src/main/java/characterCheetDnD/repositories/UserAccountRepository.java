package characterCheetDnD.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import characterCheetDnD.models.UserAccount;

import org.springframework.transaction.annotation.Transactional;

//import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {


    @Modifying
    @Query(value = "update UserAccount set userDeleted = true where id = :userId")
    void deleteUserById(@Param("userId") long userId);


    List<UserAccount> getAllByEmailAndPasswordAndUserDeleted(String email, String password, boolean isDeleted);


    List<UserAccount> getAllByEmailAndUserDeleted(String email, boolean isDeleted);

    List<UserAccount> getAllByIdAndUserDeleted(long userId,boolean flag);

    //first name

    @Modifying
    @Query(value = "update UserAccount set firstName = :newFirstName where id = :userId")
    void setNewFirstNameIntoDB(@Param("newFirstName") String newFirstName,
                               @Param("userId") long userId);

    //last name

    @Modifying
    @Query(value = "update UserAccount  set lastName = :newLastName where id = :userId")
    void setNewLastNameIntoDB(@Param("newLastName") String newLastName,
                              @Param("userId") long userId);

    //nickName

    @Modifying
    @Query(value = "update UserAccount  set nickName = :newNickName where id = :userId")
    void setNewNickNameIntoDB(@Param("newNickName") String newNickName,
                              @Param("userId") long userId);

    //email

    @Modifying
    @Query(value = "update UserAccount set email = :newEmail where id = :userId")
    void setNewEmailIntoDB(@Param("newEmail") String newEmail,
                           @Param("userId") long userId);

    //password

    @Modifying
    @Query(value = "update UserAccount set password = :newPassword where id = :userId")
    void setNewPasswordIntoDB(@Param("newPassword") String newPassword,
                              @Param("userId") long userId);

}
