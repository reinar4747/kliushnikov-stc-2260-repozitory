package characterCheetDnD.repositories;

import characterCheetDnD.models.Monster;
import characterCheetDnD.models.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public interface MonsterRepository extends JpaRepository<Monster, Long> {

    List<Monster> getAllById(long monsterId);

    List<Monster> getAllByMonsterName(String monsterName);

    List<Monster> getAllByUserAccountsAndMonsterDeleted(UserAccount userAccount,boolean flag);

    @Modifying
    @Query(value =  "update Monster set monsterDeleted = :deleted where id = :monsterId")
    void setMonsterOnAndOff(@Param("deleted") boolean flag,
                            @Param("monsterId") long monsterId);

    List<Monster> getAllByMonsterType(String monsterType);


//------------------------Stats-------------------------------
    @Modifying
    @Query(value = "update Monster set monsterName = :monsterName where id = :monsterId")
    void setMonsterNameIntoDB(@Param("monsterName") String monsterName,
                              @Param("monsterId") long monsterId);
    @Modifying
    @Query(value = "update Monster set monsterType = :monsterType where id = :monsterId")
    void setMonsterTypeIntoDB(@Param("monsterType") String monsterType,
                              @Param("monsterId") long monsterId);
    @Modifying
    @Query(value = "update Monster set monsterStrength = :str where id = :monsterId")
    void setMonsterStrIntoDB(@Param("str") String monsterStr,
                             @Param("monsterId") long monsterId);
    @Modifying
    @Query(value = "update Monster set monsterDexterity = :dex where id = :monsterId")
    void setMonsterDexIntoDB(@Param("dex") String monsterDex,
                             @Param("monsterId") long monsterId);
    @Modifying
    @Query(value = "update Monster set monsterConstitution = :con where id = :monsterId")
    void setMonsterConIntoDB(@Param("con") String monsterCon,
                             @Param("monsterId") long monsterId);
    @Modifying
    @Query(value = "update Monster set monsterIntelligence = :int where id = :monsterId")
    void setMonsterIntIntoDB(@Param("int") String monsterInt,
                             @Param("monsterId") long monsterId);
    @Modifying
    @Query(value = "update Monster set monsterWisdom = :wis where id = :monsterId")
    void setMonsterWisIntoDB(@Param("wis") String monsterWis,
                             @Param("monsterId") long monsterId);
    @Modifying
    @Query(value = "update Monster set monsterCharisma = :cha where id = :monsterId")
    void setMonsterChaIntoDB(@Param("cha") String monsterCha,
                             @Param("monsterId") long monsterId);
    @Modifying
    @Query(value = "update Monster set monsterDescription = :description where id = :monsterId")
    void setMonsterDescriptionIntoDB(@Param("description") String monsterDescription,
                                     @Param("monsterId") long monsterId);

    @Modifying
    @Query(value = "delete from Monster where id = :monsterId")
    void deleteMonstersById(@Param("monsterId") long monsterId);


    void deleteAllById(long id);


}
