package characterCheetDnD.repositories;

import characterCheetDnD.models.Calculator;
import characterCheetDnD.models.GameCharacter;
import characterCheetDnD.models.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface CharacterRepository extends JpaRepository<GameCharacter, Long> {

  List<GameCharacter> getAllById(long id);
  List<GameCharacter> getAllByUserAccountAndCharacterDeleted(UserAccount userAccount,boolean flag);

@Modifying
@Query(value = "update GameCharacter set characterDeleted = :deleted where id = :charId")
void setCharacterOnAnDOff(@Param("deleted") boolean flag,
                          @Param("charId") long characterId);

  @Modifying   // default
  @Query(value = "update GameCharacter set " +
          "name = '-'," +
          "race = '-'," +
          "className = '-'," +
          "experience = 0," +
          "hitPoints = 0," +
          "level = 1," +
          "weapon = '-'," +
          "armour = '-'," +
          "characterDeleted = false," +
          "inventory = '-'," +
          "userAccount = :userAccount," +
          "abilityScores = :abilityScores where id = 1L")
  void setDefaultCharactersValues(@Param("userAccount") UserAccount userAccount,
                                  @Param("abilityScores") Calculator abilityScores);// default
//-------------------------race and class -------------------

  @Modifying
  @Query(value = "update GameCharacter set race = :newRace where id = 1L")
  void setCharacterRaceIntoDB(@Param("newRace") String race);


  @Modifying
  @Query(value = "update GameCharacter set className = :newClass where id = 1L")
  void setCharacterClassIntoDB(@Param("newClass") String newClass);

  //------------------ LvL and Experience and hitPoints------------
  @Modifying
  @Query(value = "update GameCharacter set level = :newLvL where id = :charId")
  void setLvLIntoDB(@Param("newLvL") int LvL,
                    @Param("charId") long characterId);

  @Modifying
  @Query(value = "update GameCharacter set experience = :experience where id = :charId")
  void setExperienceIntoDB(@Param("experience") int experience,
                           @Param("charId") long characterId);

  @Modifying
  @Query(value = "update GameCharacter set hitPoints = :hitPoints where id = :charId")
  void setHitPointsIntoDB(@Param("hitPoints") int hitPoints,
                          @Param("charId") long characterId);
//  -------------------------------weapon and armor ----------------------

  @Modifying
  @Query(value = "update GameCharacter set weapon = :newWeapon where id = :charId")
  void setCharacterWeaponIntoDB(@Param("newWeapon") String weapon,
                                @Param("charId") long characterId);

  @Modifying
  @Query(value = "update GameCharacter set armour = :newArmour where id = :charId")
  void setCharacterArmorIntoDB(@Param("newArmour") String armor,
                               @Param("charId") long characterId);

//----------------------------------name ---------------------------------

  @Modifying
  @Query(value = "update GameCharacter set name = :characterName where id = :charId")
  void setCharacterNameIntoDB(@Param("characterName") String name,
                              @Param("charId") long characterId);

  //  -------------------------inventory---------------------
  @Modifying
  @Query(value = "update GameCharacter set inventory = :inventory where id = :charId")
  void setInventoryIntoDB(@Param("inventory") String newItem,
                          @Param("charId") long characterId);

  @Modifying
  @Query(value = "select character.inventory from GameCharacter character where character.id = :charId")
  List<String> getInventoryById(@Param("charId") long characterId);


}
