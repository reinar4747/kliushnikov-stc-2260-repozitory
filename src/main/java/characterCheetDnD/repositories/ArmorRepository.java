package characterCheetDnD.repositories;

import characterCheetDnD.models.Armor;
import characterCheetDnD.models.GameCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ArmorRepository extends JpaRepository<Armor, Long> {
    List<Armor> getAllByGameCharacter(GameCharacter gameCharacter);

    List<Armor> getAllByArmorName(String armorName);

    List<Armor> getAllByArmorId(long id);

    List<Armor> getAllByArmorWeight(String weight);


}
