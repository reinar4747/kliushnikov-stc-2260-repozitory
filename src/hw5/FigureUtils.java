package hw5;

import java.util.Random;
import java.util.Scanner;

public final class FigureUtils {

    public static final int UPPER_BOUND_COORDINATE = 20;

    public static Scanner getScanner() {
        return new Scanner(System.in);
    }

    private static Random getRandom() {
        return new Random();
    }

    public static int[] changeCoordinates(int x, int y, int upperBound) {
        x += getRandom().nextInt(upperBound);
        y += getRandom().nextInt(upperBound);
        return new int[]{x, y};
    }


}
