
package hw5;

public interface Movable {

    void move(int coordinateX, int coordinateY);
}
