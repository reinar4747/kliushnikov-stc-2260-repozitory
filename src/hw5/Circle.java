
package hw5;

public class Circle extends Ellipse implements Movable {

    public Circle(int shortRadius) {
        super(shortRadius);
    }

    @Override
    int getPerimeter() {

        return (int) (2 * Math.PI * getShortRadiusEllipse());
    }

    public void move(int coordinateX, int coordinateY) {   // Ведение координат при вызове метода.
        int[] finaleCoordinates =
                FigureUtils.changeCoordinates(coordinateX, coordinateY, FigureUtils.UPPER_BOUND_COORDINATE);
        System.out.println(String.format(" Old X = %d,\n Old Y = %d,\n New X = %d,\n New Y = %d",
                coordinateX, coordinateY, finaleCoordinates[0], finaleCoordinates[1]));
    }
}
