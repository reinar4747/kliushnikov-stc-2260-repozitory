
package hw5;

public abstract class Figure {

    abstract int getPerimeter();
}
