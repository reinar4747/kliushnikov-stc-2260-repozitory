
package hw5;


public class Square extends Rectangle implements Movable {

    public Square(int height, int length) {
        super(height, length);
    }

    @Override
    public int getPerimeter() {
        return getLength() * 4;
    }

    public void move(int coordinateX, int coordinateY) {     // Ведение координат при вызове метода.
        int[] finaleCoordinates =
                FigureUtils.changeCoordinates(coordinateX, coordinateY, FigureUtils.UPPER_BOUND_COORDINATE);
        System.out.println(String.format(" Old X = %d,\n Old Y = %d,\n New X = %d,\n New Y = %d",
                coordinateX, coordinateY, finaleCoordinates[0], finaleCoordinates[1]));
    }
}
