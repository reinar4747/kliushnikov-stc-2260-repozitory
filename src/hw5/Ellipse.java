package hw5;

public class Ellipse extends Figure {
    private int longRadiusEllipse;
    private int shortRadiusEllipse;

    public Ellipse(int longRadius, int shortRadius) {
        this.longRadiusEllipse = longRadius;
        this.shortRadiusEllipse = shortRadius;
    }

    public Ellipse(int shortRadius) {
        this.shortRadiusEllipse = shortRadius;
    }

    @Override
    int getPerimeter() {

        return (int) (4 * (((Math.PI * longRadiusEllipse * shortRadiusEllipse) +
                (Math.pow((longRadiusEllipse - shortRadiusEllipse), 2)))
                / (longRadiusEllipse + shortRadiusEllipse)));
    }

    public int getShortRadiusEllipse() {
        return shortRadiusEllipse;
    }

    public void setShortRadiusEllipse(int shortRadiusEllipse) {
        this.shortRadiusEllipse = shortRadiusEllipse;
    }

    public int getLongRadiusEllipse() {
        return longRadiusEllipse;
    }

    public void setLongRadiusEllipse(int longRadiusEllipse) {
        this.longRadiusEllipse = longRadiusEllipse;
    }

}
