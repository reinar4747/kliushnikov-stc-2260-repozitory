package hw5;


public class Rectangle extends Figure {
    private int height;
    private int length;

    public Rectangle(int height, int length) {
        this.length = length;
        this.height = height;
    }

    @Override
    public int getPerimeter() {

        return 2 * (length + height);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

}
