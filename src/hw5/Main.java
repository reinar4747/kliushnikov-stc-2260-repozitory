
package hw5;

public class Main {

    public static void main(String[] args) {

        Figure rectangle = new Rectangle(3, 5);
        System.out.println("Rectangle perimeter: " + rectangle.getPerimeter());

        Figure square = new Square(5, 5);
        System.out.println("Square perimeter: " + square.getPerimeter());

        Figure ellipse = new Ellipse(10, 5);
        System.out.println("Ellipse circumference: " + ellipse.getPerimeter());

        Figure circle = new Circle(5);
        System.out.println("Circle circumference: " + circle.getPerimeter());


        Figure[] figures = {rectangle, square, ellipse, circle};
        for (int i = 0; i < figures.length; i++) {
            figures[i].getPerimeter();
        }

        Movable movableSquare = new Square(5, 5);
        Movable movableCircle = new Circle(5);

        Movable[] move = {movableCircle, movableSquare};

        for (Movable moveFigure : move) {
            moveFigure.move(5, 8);
        }

        movableSquare.move(FigureUtils.getScanner().nextInt(), FigureUtils.getScanner().nextInt());

    }
}
