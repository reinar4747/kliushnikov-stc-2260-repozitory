package hw3;

import java.util.Arrays;
import java.util.Scanner;

public class HW3 {
    /*
    Написать программу, которая высчитывает наибольшую цифру в числе.

* **Пример 1:**
  Пользователь ввел число - 149\
  Программа должна вывести в консоль - 9

* **Пример 2:**
  Пользователь ввел число - 2050\
  Программа должна вывести в консоль - 5

* **Пример 3:**
  Пользователь ввел число - -10076\
  Программа должна вывести в консоль - 7
     */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ведите число : ");
        int number = scanner.nextInt();
        int numberClon = number;
        int count = 0;
        while (Math.abs(numberClon) > 0) {
            numberClon = numberClon / 10;
            count++;
        }
        int[] numberArr = new int[count];
        for (int i = 0; i < count; i++) {
            numberArr[i] = number % 10;
            number = number / 10;
        }
        Arrays.sort(numberArr);
        System.out.println(numberArr[numberArr.length - 1]);

    }
}

