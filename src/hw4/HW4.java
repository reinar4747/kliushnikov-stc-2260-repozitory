package hw4;

import java.util.Arrays;

public class HW4 {

    //Реализовать функцию, принимающую на вход массив и целое число.
    //Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
    public static int searchIndex(int[] arr, int number) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == number) {

                return i;
            }
        }

        return -1;
    }

    /*
    Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
    Было:
    34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
    Стало после применения процедуры:
    34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
    */
    public static int[] numbersToLeft(int[] arr) {
        int index = 0;
        int[] arrResult = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                arrResult[index] = arr[i];
                index++;
            }
        }

        return arrResult;
    }

    public static void main(String[] args) {
        int[] test = {1, 2, 3, 4, 5};
        System.out.println(searchIndex(test, 5));
        int[] test2 = {1, 0, 2, 0, 3, 8, 4, 0, 0, 5, 6};
        System.out.println(Arrays.toString(numbersToLeft(test2)));


    }


}
