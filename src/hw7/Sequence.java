package hw7;

import java.util.Arrays;
import java.util.Random;

public class Sequence {
    //    public static int calc(int number1, int number2) {
//        ByCondition byCondition = (x, y) -> x + y;
//        return byCondition.sum(number1,number2);
//    }
    public static int[] randomArray(int length) {
        Random random = new Random();
        int[] resultArray = new int[length];
        for (int i = 0; i < length; i++) {
            resultArray[i] = random.nextInt(100) + 1;
        }

        return resultArray;
    }
    /*
    В main в качестве condition подставить:

проверку на четность элемента
проверку, является ли сумма цифр элемента четным числом.
Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
Доп. задача: проверка на палиндромность числа (число читается одинаково и слева,
       и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).
     */

    public static int[] filter(int[] array, ByCondition condition) {
        int[] randomArray = array;
        System.out.println("Original array : " + Arrays.toString(randomArray));
        int index = 0;
        int[] result = new int[array.length];
        for (int i = 0; i < result.length; i++) {
            if (condition.isOk(array[i])) {
                result[index] = array[i];
                index++;
            }
        }

        return Arrays.copyOf(result, index);
    }


    public static void main(String[] args) {
        // проверка на четность элемента
        ByCondition evenElements;
        evenElements = x -> x % 2 == 0;
        System.out.println("Array of even numbers : " +
                Arrays.toString(filter(randomArray(10), evenElements)));

        // проверкa, является ли сумма цифр элемента четным числом.
        ByCondition sumEvenElementsIsOk;
        sumEvenElementsIsOk = x -> {
            int sum = 0;
            while (Math.abs(x) > 0) {
                sum += x % 10;
                x /= 10;
            }

            return sum % 2 == 0;
        };

        System.out.println("Array of summ even numbers : " +
                Arrays.toString(filter(randomArray(10), sumEvenElementsIsOk)));

        //Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
        ByCondition allEvenElementsIsOk;
        allEvenElementsIsOk = x -> {
            while (Math.abs(x) > 0) {
                if ((x % 10) % 2 != 0) {

                    return false;
                }
                x /= 10;
            }

            return true;
        };

//        int[] test = {11, 22, 33, 444, 555, 666};
        System.out.println("Array of numbers with even digits : " +
                Arrays.toString(filter(randomArray(10), allEvenElementsIsOk)));

        //Доп. задача: проверка на палиндромность числа (число читается одинаково и слева,
        //       и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).
        ByCondition palindrome;
        palindrome = x -> {
            String xString = String.valueOf(x);
            String[] arrString = xString.split("");
            StringBuilder revertX = new StringBuilder();
            for (int i = arrString.length - 1; i >= 0; i--) {
                revertX.append(arrString[i]);
            }

            return xString.contentEquals(revertX);
        };
        System.out.println("Array of palindromes : " +
                Arrays.toString(filter(randomArray(10), palindrome)));


    }
}
