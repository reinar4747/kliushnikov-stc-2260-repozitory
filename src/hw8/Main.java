package hw8;

public class Main {
    public static void main(String[] args) {
        Person person1 = Person.builder()
                .name("Vasa")
                .lastName("Pypkin")
                .patronymic("Vasilivich")
                .city("Java")
                .street("Programmers St.")
                .house("12")
                .flat("234")
                .numberPassport("32 56 567982")
                .build();

        System.out.println(person1.toString());
        System.out.println("*****************************************");

        Person person2 = Person.builder()
                .name("Vasa")
                .lastName("Pypkin")
                .patronymic("Vasilivich")
                .city("Java")
                .street("Programmers St.")
                .house("12")
                .flat("234")
                .numberPassport("32 56 567982")
                .build();

        System.out.println("Equals two objects (person1 and person2) : " + person1.equals(person2));

        System.out.printf("Equals two object's hashCodes (person1 and person2) : %b", person1.hashCode() == person2.hashCode());

    }


}
