package hw8;

import java.util.Objects;

public class Person {
    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String numberPassport;

    public Person(String name,
                  String lastName,
                  String patronymic,
                  String city,
                  String street,
                  String house,
                  String flat,
                  String numberPassport) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.numberPassport = numberPassport;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public void setNumberPassport(String numberPassport) {
        this.numberPassport = numberPassport;
    }

    public String getNumberPassport() {
        return numberPassport;
    }

    public String toString() {
        String[] getNumberPassport = numberPassport.split(" ");
        return String.format("%s %s %s" +
                        "\n Passport:" +
                        "\n Serial Number: %s %s Number: %s" +
                        "\n City %s,Street %s,house %s,flat %s",
                name, lastName, patronymic,
                getNumberPassport[0],
                getNumberPassport[1],
                getNumberPassport[2],
                city, street, house, flat);
    }

    @Override
    public boolean equals(Object personToPassportEquals) {
        if (this == personToPassportEquals) return true;
        if (personToPassportEquals == null || getClass() != personToPassportEquals.getClass()) return false;
        Person person = (Person) personToPassportEquals;
        return numberPassport.equals(person.numberPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberPassport);
    }

    public static PersonBuilder builder() {
        return new PersonBuilder(new Person());
    }

    public static class PersonBuilder {
        private Person person;

        public PersonBuilder(Person person) {
            this.person = person;
        }


        public PersonBuilder name(String name) {
            person.setName(name);
            return this;
        }

        public PersonBuilder lastName(String lastName) {
            person.setLastName(lastName);
            return this;
        }

        public PersonBuilder patronymic(String patronymic) {
            person.setPatronymic(patronymic);
            return this;
        }

        public PersonBuilder city(String city) {
            person.setCity(city);
            return this;
        }

        public PersonBuilder street(String street) {
            person.setStreet(street);
            return this;
        }

        public PersonBuilder house(String house) {
            person.setHouse(house);
            return this;
        }

        public PersonBuilder flat(String flat) {
            person.setFlat(flat);
            return this;
        }

        public PersonBuilder numberPassport(String numberPassport) {
            person.setNumberPassport(numberPassport);
            return this;
        }

        public Person build() {
            return person;
        }


    }
}
