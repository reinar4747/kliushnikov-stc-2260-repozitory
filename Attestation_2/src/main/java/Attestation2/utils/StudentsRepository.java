package Attestation2.utils;

import Attestation2.persons.Student;

import java.util.List;

public interface StudentsRepository {
    List<Student> findStudent(String PersonName);

    List<Student> findStudent(String personFirstName, String personLastName);
}
