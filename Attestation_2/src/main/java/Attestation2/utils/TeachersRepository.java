package Attestation2.utils;

import Attestation2.persons.Teacher;

import java.util.List;

public interface TeachersRepository {
    List<Teacher> findTeacher(String studentOneName);

    List<Teacher> findTeacher(String studentFirstName, String studentLastName);
}
