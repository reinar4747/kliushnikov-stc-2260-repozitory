package Attestation2.utils;

import Attestation2.persons.Student;
import Attestation2.persons.Teacher;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class CreateObjects {
    public static void main(String[] args) {

        try (Session session = new Configuration().configure().buildSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();


            Teacher teacher1 = Teacher.builder()
                    .firstName("Nikolay")
                    .lastName("Petrovich")
                    .build();
            Teacher teacher2 = Teacher.builder()
                    .firstName("Igor")
                    .lastName("Vasiltvich")
                    .build();
            Teacher teacher3 = Teacher.builder()
                    .firstName("Feodor")
                    .lastName("Ivanovich")
                    .build();

            Student student1 = Student.builder()
                    .firstName("Vasa")
                    .lastName("Pupkin")
                    .build();
            Student student2 = Student.builder()
                    .firstName("Artem")
                    .lastName("Anisimov")
                    .build();
            Student student3 = Student.builder()
                    .firstName("Vera")
                    .lastName("Petrova")
                    .build();


            teacher1.getStudents().add(student1);
            teacher1.getStudents().add(student2);
            teacher2.getStudents().add(student1);
            teacher3.getStudents().add(student1);
            teacher3.getStudents().add(student2);
            teacher3.getStudents().add(student3);

            session.save(teacher1);
            session.save(teacher2);
            session.save(teacher3);

            transaction.commit();


            session.close();

        }
    }
}
