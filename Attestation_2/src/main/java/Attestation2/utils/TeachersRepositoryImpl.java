package Attestation2.utils;

import Attestation2.persons.Teacher;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class TeachersRepositoryImpl implements TeachersRepository {
    private Session session;

    private static final String SQL_FIND_TEACHERS = "select * from teacher\n" +
            "    join teacher_student ts on teacher.id = ts.teachers_id\n" +
            "    join student s on s.id = ts.students_id\n" +
            "    where s.first_name = :studentFirstName or s.last_name = :studentLastName";

    public TeachersRepositoryImpl(Session session) {
        this.session = session;
    }

    @Override
    public List<Teacher> findTeacher(String studentOneName) {
        Transaction transaction = session.beginTransaction();

        List<Teacher> result;
        result = session
                .createNativeQuery(SQL_FIND_TEACHERS, Teacher.class)
                .setParameter("studentFirstName", studentOneName)
                .setParameter("studentLastName", studentOneName)
                .list();
        transaction.commit();

        return result;
    }

    @Override
    public List<Teacher> findTeacher(String studentFirstName, String studentLastName) {
        Transaction transaction = session.beginTransaction();

        List<Teacher> result;
        result = session
                .createNativeQuery(SQL_FIND_TEACHERS, Teacher.class)
                .setParameter("studentFirstName", studentFirstName)
                .setParameter("studentLastName", studentLastName)
                .list();
        transaction.commit();

        return result;
    }
}
