package Attestation2.utils;

import Attestation2.persons.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class StudentsRepositoryImpl implements StudentsRepository {
    private Session session;
    private static final String SQL_FIND_STUDENTS =
            "select * from student " +
                    "join teacher_student on student.id = teacher_student.students_id " +
                    "join teacher on teacher.id = teacher_student.teachers_id "
                    + "where teacher.first_name = :teacherFirstName or teacher.last_name = :teacherLastName";

    public StudentsRepositoryImpl(Session session) {
        this.session = session;
    }

    @Override
    public List<Student> findStudent(String teacherOneName) {
        Transaction transaction = session.beginTransaction();

        List<Student> result;
        result = session
                .createNativeQuery(SQL_FIND_STUDENTS, Student.class)
                .setParameter("teacherFirstName", teacherOneName)
                .setParameter("teacherLastName", teacherOneName)
                .list();
        transaction.commit();

        return result;
    }

    @Override
    public List<Student> findStudent(String teacherFirstName, String teacherLastName) {
        Transaction transaction = session.beginTransaction();

        List<Student> result;
        result = session
                .createNativeQuery(SQL_FIND_STUDENTS, Student.class)
                .setParameter("teacherFirstName", teacherFirstName)
                .setParameter("teacherLastName", teacherLastName)
                .list();
        transaction.commit();

        return result;
    }
}
