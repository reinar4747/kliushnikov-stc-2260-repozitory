package Attestation2;

import Attestation2.utils.StudentsRepository;
import Attestation2.utils.StudentsRepositoryImpl;
import Attestation2.utils.TeachersRepository;
import Attestation2.utils.TeachersRepositoryImpl;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {
        try (Session session = new Configuration().configure().buildSessionFactory().openSession();) {


            StudentsRepository studentsRepository = new StudentsRepositoryImpl(session);
            TeachersRepository teachersRepository = new TeachersRepositoryImpl(session);

            System.out.println(studentsRepository.findStudent("Nikolay"));
            System.out.println(studentsRepository.findStudent("Nikolay", "Petrovich"));
            System.out.println(teachersRepository.findTeacher("Vasa"));
            System.out.println(teachersRepository.findTeacher("Vasa", "Pupkin"));

        }
    }
}
